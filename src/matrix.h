#ifndef MATRIX_H
#define MATRIX_H

#include <stdbool.h>
#define SYM 's'

/* EXAMPLE 1: MATRIX -> MATRIX_MSR
 * [
 *     [1, 2, 0, 1]
 *     [0, 6, 0, 7]
 *     [3, 0, 9, 1]
 *     [8, 0, 0, 4]
 * ]
 *
 * based on above matrix of type MATRIX MATRIX_MSR
 * can be derived.
 *
 * meta -> [5, 7, 9, 10, 11, 1, 3, 3, 0, 3, 0]
 *          ---csr el----  ----crs cols----
 * v    -> [1, 6, 9, 4, 0, 2, 1, 7, 3, 1, 8]
 *          ---diag---  x  ----off diag----
 *
 * csr el:   pointers to where the next row starts in the off diag portion of
 * the v csr cols: col indices of non-zero off-diag elements
 *
 * diag:     part of the array is always of the dimension N of the NxN matrix
 * x:        is a dummy element used for iteration convenience
 * offdiag:  non-zero off-diag elements
 *
 *
 * EXAMPLE 2: MATRIX -> MATRIX_MSR (with symmetry invoked)
 * [
 *     [1, 0, 3, 1]
 *     [0, 6, 0, 7]
 *     [3, 0, 0, 1]
 *     [1, 7, 1, 4]
 * ]
 *
 * meta -> [5, 7, 8, 9, 9, 2, 3, 3, 3]
 *          ---csr el---- --csr cols--
 * v    -> [1, 6, 0, 4, 0, 3, 1, 7, 1]
 *          ---diag---  x --off diag--
 * */

typedef struct {
  int dim_r, dim_c;
  double **m;
} MATRIX;

typedef struct {
  bool f_sym;
  int matrix_dim;
  int msr_dim;
  int *meta;
  double *val;
} MATRIX_MSR;

void cleanup_msr(MATRIX_MSR *matrix_msr);
MATRIX_MSR *allocate_msr(bool f_sym, int msr_dim, int matrix_dim);
void cleanup_matrix(MATRIX *matrix);
MATRIX *allocate_matrix(int dim_r, int dim_c);

MATRIX_MSR *
parse_matrix_msr(char *file_name); // See docs/msr_read_annotation.pdf
MATRIX *msr_to_matrix(MATRIX_MSR *matrix_msr);

/*
 * Realizes Mx = y operation (where M is MSR format)
 * Two main interpretations are heavily utilized:
 * - inner product between matrix rows and vector
 * - linear combination of matrix columns according to vector values
 *
 *
 * EXAMPLE 1: MATRIX -> MATRIX_MSR
 * [
 *     [1, 2, 0, 1]
 *     [0, 6, 0, 7]
 *     [3, 0, 9, 1]
 *     [8, 0, 0, 4]
 * ]
 *
 * x: [1, 2, 3, 4]
 * y: [8, 40, 34, 24]
 *
 *
 * EXAMPLE 2: MATRIX -> MATRIX_MSR (with symmetry invoked)
 * [
 *     [1, 0, 3, 1]
 *     [0, 6, 0, 7]
 *     [3, 0, 0, 1]
 *     [1, 7, 1, 4]
 * ]
 *
 * x: [1, 2, 3, 4]
 * y: [14, 40, 7, 34]
 * */
MATRIX *transpose(MATRIX *m);
double *matvec_msr(MATRIX_MSR *m, double *x, double *y);
double *matvec(MATRIX *m, double *x, double *y);

double inner_product(double *u, double *v, int dim);
double normalize_vec(double *v, double dim);
#endif
