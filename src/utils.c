#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

void print_vector_int(int *vec, int dim) {

  for (int j = 0; j < dim; j++) {
    if (j == dim - 1)
      printf("%d]", vec[j]);
    else if (j == 0)
      printf("[%d,  ", vec[j]);
    else
      printf("%d,  ", vec[j]);
  }
  printf("\n");
}

void print_vector(double *vec, int dim) {
  for (int j = 0; j < dim; j++) {
    if (j == dim - 1)
      printf("%lf]\n", vec[j]);
    else if (j == 0)
      printf("[%lf,  ", vec[j]);
    else
      printf("%lf,  ", vec[j]);
  }
}

void print_matrix(double **matrix, int dim_r, int dim_c) {
  printf("[\n");
  for (int i = 0; i < dim_r; i++) {
    print_vector(matrix[i], dim_c);
  }
  printf("]\n");
}

void print_vector_T(double *vec, int dim) {
  for (int j = 0; j < dim; j++) {
    if (j == dim - 1)
      printf("%lf]\n", vec[j]);
    else if (j == 0)
      printf("[%lf\n", vec[j]);
    else
      printf("%lf\n,  ", vec[j]);
  }
}

void print_vector_int_T(int *vec, int dim) {
  for (int j = 0; j < dim; j++) {
    if (j == dim - 1)
      printf("%d]\n", vec[j]);
    else if (j == 0)
      printf("[%d\n", vec[j]);
    else
      printf("%d\n,  ", vec[j]);
  }
}

void combine_vectors_inplace(double *a, double *b, double *c, double s_b,
                             double s_c, int dim) {
  /*
   * a[i] <- s_b*b[i] + s_c*c[i]
   */
  for (int i = 0; i < dim; i++)
    a[i] = s_b * b[i] + s_c * c[i];
}

double *add_vectors(double *a, double *b, double *c, int dim) {
  for (int i = 0; i < dim; i++)
    c[i] = a[i] + b[i];
  return c;
}

double *substr_vectors(double *a, double *b, double *c, int dim) {
  for (int i = 0; i < dim; i++)
    c[i] = a[i] - b[i];
  return c;
}

void print_matrix_T(double **matrix, int dim_r, int dim_c) {
  printf("[\n");
  for (int j = 0; j < dim_c; j++) {
    printf("[");
    for (int i = 0; i < dim_r; i++) {
      if (i == dim_r - 1)
        printf("%lf", matrix[i][j]);
      else
        printf("%lf, ", matrix[i][j]);
    }
    printf("]\n");
  }
  printf("]\n");
}

double *get_filled_vector(int dim, double val) {
  double *v = (double *)malloc(sizeof(double) * dim);
  for (int i = 0; i < dim; i++)
    v[i] = val;

  return v;
}

double *get_zero_vector(int dim) { return get_filled_vector(dim, 0.0); }

void copy_vector(double *from, double *to, int dim) {
  for (int i = 0; i < dim; i++)
    to[i] = from[i];
}
