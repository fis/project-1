#include "matrix.h"
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void cleanup_msr(MATRIX_MSR *matrix_msr) {
  free(matrix_msr->meta);
  free(matrix_msr->val);
  free(matrix_msr);
}

MATRIX_MSR *allocate_msr(bool f_sym, int msr_dim, int matrix_dim) {
  MATRIX_MSR *matrix_msr = (MATRIX_MSR *)malloc(sizeof(MATRIX_MSR));
  matrix_msr->meta = (int *)malloc(sizeof(int) * msr_dim);
  matrix_msr->val = (double *)malloc(sizeof(double) * msr_dim);

  matrix_msr->matrix_dim = matrix_dim;
  matrix_msr->msr_dim = msr_dim;
  matrix_msr->f_sym = f_sym;

  return matrix_msr;
}

void cleanup_matrix(MATRIX *matrix) {
  for (int r = 0; r < matrix->dim_r; r++)
    free(matrix->m[r]);
  free(matrix->m);
  free(matrix);
}

MATRIX *allocate_matrix(int dim_r, int dim_c) {
  MATRIX *matrix = (MATRIX *)malloc(sizeof(MATRIX));

  double **m = (double **)malloc(sizeof(double *) * dim_r);
  for (int r = 0; r < dim_r; r++) {
    m[r] = (double *)malloc(sizeof(double) * dim_c);
    for (int c = 0; c < dim_c; c++)
      m[r][c] = 0.0;
  }

  matrix->m = m;
  matrix->dim_r = dim_r;
  matrix->dim_c = dim_c;

  return matrix;
}

MATRIX_MSR *parse_matrix_msr(char *file_name) {
  FILE *f;

  if ((f = fopen(file_name, "r")) == NULL) {
    printf("Could not locate the file with name %s\n", file_name);
    return NULL;
  } else {
    // Getting the symmetry flag
    char sym_label;
    bool f_sym;
    if (fscanf(f, " %c", &sym_label) == 0) {
      printf("Could not parse symmetry flag\n");
      return NULL;
    }
    f_sym = sym_label == SYM;

    // Getting the dimensions
    int matrix_dim, msr_dim;
    if (fscanf(f, "\t\t%d\t\t%d", &matrix_dim, &msr_dim) == 0) {
      printf("Could not parse matrix dimensions\n");
      return NULL;
    }

    MATRIX_MSR *matrix_msr = allocate_msr(f_sym, msr_dim, matrix_dim);
    int iter = 0;
    int idx;
    double val;
    while (fscanf(f, "\t\t%d  %lf", &idx, &val) != EOF) {
      matrix_msr->meta[iter] = idx - 1;
      matrix_msr->val[iter] = val;
      iter++;
    }

    fclose(f);

    return matrix_msr;
  }
}

MATRIX *msr_to_matrix(MATRIX_MSR *matrix_msr) {
  bool f_sym = matrix_msr->f_sym;
  int dim = matrix_msr->matrix_dim;
  MATRIX *matrix = allocate_matrix(dim, dim);

  for (int i = 0; i < dim; i++) {
    int non_zero_start = matrix_msr->meta[i];
    int non_zero_end = matrix_msr->meta[i + 1];
    for (int k = non_zero_start; k < non_zero_end; k++) {
      double val = matrix_msr->val[k];
      int j = matrix_msr->meta[k];

      matrix->m[i][j] = val;
      if (f_sym)
        matrix->m[j][i] = val;
    }
    matrix->m[i][i] = matrix_msr->val[i];
  }

  return matrix;
}

double *matvec(MATRIX *m, double *x, double *y) {
  int dim_r = m->dim_r;
  int dim_c = m->dim_c;

  for (int i = 0; i < dim_r; i++)
    for (int j = 0; j < dim_c; j++)
      y[i] += m->m[i][j] * x[j];

  return y;
}

double *matvec_msr(MATRIX_MSR *m, double *x, double *y) {
  int dim = m->matrix_dim;
  int msr_dim = m->msr_dim;

  // Linear combination of diagonal column vectors
  for (int diag = 0; diag < dim; diag++)
    y[diag] = m->val[diag] * x[diag];

  for (int i = 0; i < dim; i++) {
    int non_zero_start = m->meta[i];
    int non_zero_end = m->meta[i + 1];
    for (int k = non_zero_start; k < non_zero_end; k++) {
      double val = m->val[k];
      int j = m->meta[k];

      // Inner product of non-zero row elements with vector x
      y[i] += val * x[j];

      if (m->f_sym)
        // Linear combination of off-diagonal column vectors
        y[j] += val * x[i];
    }
  }

  return y;
}

double inner_product(double *u, double *v, int dim) {
  double res = 0.0;
  for (int i = 0; i < dim; i++)
    res += u[i] * v[i];
  return res;
}

double normalize_vec(double *v, double dim) {
  double norm = sqrt(inner_product(v, v, dim));
  for (int i = 0; i < dim; i++)
    v[i] /= norm;
  return norm;
}

MATRIX *transpose(MATRIX *m) {
  int dim_r = m->dim_r;
  int dim_c = m->dim_c;
  MATRIX *m_T = allocate_matrix(dim_c, dim_r);
  for (int i = 0; i < dim_r; i++)
    for (int j = 0; j < dim_c; j++)
      m_T->m[j][i] = m->m[i][j];
  return m_T;
}
