#ifndef GIVENS_H

#define GIVENS_H

typedef struct {
  double cos, sin;
} GIVENS_ROTATION;

#endif
