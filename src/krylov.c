#include "krylov.h"
#include "matrix.h"
#include "utils.h"
#include <math.h>
#include <stdlib.h>

KRYLOV_SPACE *allocate_krylov(MATRIX_MSR *A, double *r_0, int m) {
  int dim = A->matrix_dim;
  KRYLOV_SPACE *K_m = (KRYLOV_SPACE *)malloc(sizeof(KRYLOV_SPACE));
  K_m->m = m;
  K_m->A = A;
  K_m->W = allocate_matrix(m + 1, dim);

  // Implicitly initialize 0th component
  copy_vector(r_0, K_m->W->m[0], dim);
  normalize_vec(K_m->W->m[0], dim);

  return K_m;
}

void cleanup_krylov(KRYLOV_SPACE *K_m) {
  cleanup_matrix(K_m->W);
  free(K_m);
}

MATRIX *allocate_hessenberg(int m) { return allocate_matrix(m + 1, m); }
int compute_next_krylov_vector(KRYLOV_SPACE *K_m, MATRIX *H_m, int i) {
  int dim = K_m->A->matrix_dim;
  matvec_msr(K_m->A, K_m->W->m[i], K_m->W->m[i + 1]);

  // Remove orthogonal projections
  for (int j = 0; j < i + 1; j++) {
    H_m->m[j][i] = inner_product(K_m->W->m[j], K_m->W->m[i + 1], dim);
    for (int k = 0; k < dim; k++)
      K_m->W->m[i + 1][k] -= H_m->m[j][i] * K_m->W->m[j][k];
  }
  H_m->m[i + 1][i] = normalize_vec(K_m->W->m[i + 1], dim);

  return 0;
}
