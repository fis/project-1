#include "gmres.h"
#include "givens.h"
#include "krylov.h"
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double compute_residual(double *b, double *y, int dim) {
  double *r = get_zero_vector(dim);
  substr_vectors(b, y, r, dim);
  double r_norm = sqrt(inner_product(r, r, dim));
  free(r);
  return r_norm;
}

double *gmres(MATRIX_MSR *A, double *x_0, double *b, int m) {
  int dim = A->matrix_dim;
  double *y_0 = get_zero_vector(dim);
  matvec_msr(A, x_0, y_0);

  // Compute initial residual
  double *r_0 = get_zero_vector(dim);
  substr_vectors(b, y_0, r_0, dim);
  double r_0_norm = sqrt(inner_product(r_0, r_0, dim));

  // Compute initial g vector
  double *g = get_zero_vector(m + 1);
  g[0] = r_0_norm;

  // Start the CPU clock
  clock_t tic = clock();

  KRYLOV_SPACE *K_m = allocate_krylov(A, r_0, m);
  MATRIX *H_m = allocate_hessenberg(m);
  GIVENS_ROTATION *givens_rotations =
      (GIVENS_ROTATION *)malloc(sizeof(GIVENS_ROTATION) * m);

  /*
   * GMRES main body
   */
  for (int i = 0; i < m; i++) {
    compute_next_krylov_vector(K_m, H_m, i);

    double a, b, c;
    double cos, sin;

    // i) Prev Givens rotations
    for (int j = 0; j < i; j++) {
      a = H_m->m[j][i], b = H_m->m[j + 1][i];
      cos = givens_rotations[j].cos, sin = givens_rotations[j].sin;

      H_m->m[j][i] = cos * a + sin * b;
      H_m->m[j + 1][i] = -sin * a + cos * b;
    }

    // ii) Current givens rotations
    a = H_m->m[i][i], b = H_m->m[i + 1][i];
    c = sqrt(a * a + b * b);
    cos = a / c, sin = b / c;

    H_m->m[i][i] = cos * a + sin * b;
    H_m->m[i + 1][i] = -sin * a + cos * b;

    g[i + 1] = -sin * g[i];
    g[i] = cos * g[i];
    printf("%d    %lf    %lf\n", i + 1, fabs(g[i + 1]), r_0_norm);

    // Check if residual norm decreased 8 OOM
    if (fabs(g[i + 1]) / r_0_norm <= EXIT_CONDITION) {
      m = i + 1;
      break;
    }

    givens_rotations[i] = (GIVENS_ROTATION){cos, sin};
  }

  double *linear_comb_coefs = get_zero_vector(m);
  int num_inverted = 0;
  for (int i = m - 1; i >= 0; i--) {
    // Substract known values
    for (int j = m - 1; j > (m - 1) - num_inverted; j--)
      g[i] -= H_m->m[i][j] * linear_comb_coefs[j];

    // Compute inversion
    linear_comb_coefs[i] = g[i] / H_m->m[i][i];
    num_inverted++;
  }

  // Extract V_m out of V_m+1
  MATRIX *V_m = allocate_matrix(m, dim);
  for (int i = 0; i < m; i++)
    copy_vector(K_m->W->m[i], V_m->m[i], dim);

  double *x_m = get_zero_vector(dim);
  double *correction = get_zero_vector(dim);
  matvec(transpose(V_m), linear_comb_coefs, correction);
  add_vectors(x_0, correction, x_m, dim);

  // Stop the clock
  clock_t toc = clock() - tic;
  int msec = toc * 1000 / CLOCKS_PER_SEC;
  printf("T    %d\n", msec);

  // GMRES cleanup
  free(correction);
  free(givens_rotations);
  free(r_0);
  free(g);
  cleanup_matrix(H_m);
  cleanup_matrix(V_m);

  return x_m;
}
