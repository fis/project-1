#ifndef CG_H
#define CG_H

#include "matrix.h"

double *cg(MATRIX_MSR *A, double *x_0, double *b, double *x_exact, int m);

#endif
