#ifndef KRYLOV_H
#define KRYLOV_H

#include "matrix.h"

typedef struct {
  int m;
  MATRIX_MSR *A;
  /*
   * due to C storage format of arrays
   * this matrix is not in a vector column format,
   * but its transpose is.
   */
  MATRIX *W;
} KRYLOV_SPACE;

KRYLOV_SPACE *allocate_krylov(MATRIX_MSR *A, double *r_0, int m);
void cleanup_krylov(KRYLOV_SPACE *K_m);
MATRIX *allocate_hessenberg(int m);
int compute_next_krylov_vector(KRYLOV_SPACE *K_m, MATRIX *H_m, int i);

#endif
