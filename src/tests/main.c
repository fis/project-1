#include "../gmres.h"
#include "../krylov.h"
#include "../matrix.h"
#include "../utils.h"
#include <stdio.h>
#include <stdlib.h>

#define TEST_DELIM "============\n"

int test_non_sym_msr();
int test_sym_msr();
int test_msr_matvec(char *file_name);
int test_matvec();
int test_addition();

int test_krylov_space();
int test_hessenberg_matrix();

int test_givens_applied_to_hessenberg_matrix();

int main() {
  printf(TEST_DELIM);
  printf("Sym MSR\n");
  if (test_sym_msr() != 0)
    printf("Failed symmetric msr matvec\n");

  printf(TEST_DELIM);
  printf("Non-sym MSR\n");
  if (test_non_sym_msr() != 0)
    printf("Failed non-symmetric msr matvec\n");

  printf(TEST_DELIM);
  printf("Matvec\n");
  if (test_matvec() != 0)
    printf("Failed matvec\n");

  printf(TEST_DELIM);
  printf("Krylov\n");
  if (test_krylov_space() != 0)
    printf("Failed creating proper Krylov space\n");

  printf(TEST_DELIM);
  printf("Hessenberg\n");
  if (test_hessenberg_matrix() != 0)
    printf("Failed creating proper Krylov space\n");

  printf(TEST_DELIM);
  printf("Addition\n");
  if (test_addition() != 0)
    printf("Failed to add vectors\n");

  printf(TEST_DELIM);
}

int test_addition() {
  double *a = get_filled_vector(6, 1.2);
  double *b = get_filled_vector(6, -0.2);
  print_vector(add_vectors(a, b, NULL, 6), 6);
  return 0;
}

int test_hessenberg_matrix() {
  char file_name[] = "in/test_msr_non_sym.txt";
  MATRIX_MSR *A;
  if ((A = parse_matrix_msr(file_name)) == NULL)
    return -1;

  int m = 1;
  double test_r0[] = {0.1, 0.25, 0.1, 0.33};
  KRYLOV_SPACE *K_m = allocate_krylov(A, test_r0, m);
  MATRIX *H_m = allocate_hessenberg(m);
  for (int i = 0; i < m; i++)
    compute_next_krylov_vector(K_m, H_m, i);

  if (H_m->dim_r != m + 1 && H_m->dim_c != m) {
    printf("Wrong Hessenberg matrix dim\nExpected: r=%d c=%d\nObtained: r=%d "
           "c=%d\n",
           m + 1, m, H_m->dim_r, H_m->dim_c);
    return -1;
  }

  print_matrix(H_m->m, H_m->dim_r, H_m->dim_c);
  print_matrix_T(K_m->W->m, K_m->W->dim_r, K_m->W->dim_c);

  return 0;
}

int test_krylov_space() {
  char file_name[] = "in/test_msr_sym.txt";
  MATRIX_MSR *A;
  if ((A = parse_matrix_msr(file_name)) == NULL)
    return -1;

  int m = 3;
  double test_r0[] = {0.1, 0.25, 0.1, 0.33};
  KRYLOV_SPACE *K_m = allocate_krylov(A, test_r0, m);
  MATRIX *H_m = allocate_hessenberg(m);
  for (int i = 0; i < m; i++)
    compute_next_krylov_vector(K_m, H_m, i);

  for (int i = 0; i < m + 1; i++)
    for (int j = i; j < m + 1; j++)
      printf("Inner product of Krylov basis <%d | %d>: %lf\n", i, j,
             inner_product(K_m->W->m[i], K_m->W->m[j], A->matrix_dim));

  return 0;
}

int test_msr_matvec(char *file_name) {
  MATRIX_MSR *matrix_msr;
  if ((matrix_msr = parse_matrix_msr(file_name)) == NULL)
    return -1;

  print_matrix(msr_to_matrix(matrix_msr)->m, matrix_msr->matrix_dim,
               matrix_msr->matrix_dim);

  double *x = get_zero_vector(matrix_msr->matrix_dim);
  for (int i = 1; i <= matrix_msr->matrix_dim; i++)
    x[i - 1] = i;

  double *y = matvec_msr(matrix_msr, x, NULL);
  print_vector(y, matrix_msr->matrix_dim);

  cleanup_msr(matrix_msr);
  free(x);
  free(y);

  return 0;
}

int test_matvec() {
  MATRIX_MSR *matrix_msr;
  if ((matrix_msr = parse_matrix_msr("in/test_msr_non_sym.txt")) == NULL)
    return -1;

  double *x = get_zero_vector(matrix_msr->matrix_dim);
  for (int i = 1; i <= matrix_msr->matrix_dim; i++)
    x[i - 1] = i;

  double *y = matvec(msr_to_matrix(matrix_msr), x, NULL);
  print_vector(y, matrix_msr->matrix_dim);

  cleanup_msr(matrix_msr);
  free(x);
  free(y);

  return 0;
}

int test_non_sym_msr() { return test_msr_matvec("in/test_msr_non_sym.txt"); }

int test_sym_msr() { return test_msr_matvec("in/test_msr_sym.txt"); }
