#ifndef GMRES_H
#define GMRES_H

#include "matrix.h"
#define EXIT_CONDITION 1e-8

double compute_residual(double *b, double *y, int dim);
double *gmres(MATRIX_MSR *A, double *x_0, double *b, int m);

#endif
