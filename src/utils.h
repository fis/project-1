#ifndef UTILS_H
#define UTILS_H

void print_vector(double *vec, int dim);
void print_vector_int(int *vec, int dim);
void print_matrix(double **matrix, int dim_r, int dim_c);

void print_vector_T(double *vec, int dim);
void print_vector_int_T(int *vec, int dim);
void print_matrix_T(double **matrix, int dim_r, int dim_c);

double *get_filled_vector(int dim, double val);
double *get_zero_vector(int dim);
void copy_vector(double *from, double *to, int dim);
double *add_vectors(double *a, double *b, double *c, int dim);
double *substr_vectors(double *a, double *b, double *c, int dim);

/*
 * a[i] <- s_b*b[i] + s_c*c[i]
 */
void combine_vectors_inplace(double *a, double *b, double *c, double s_b,
                             double s_c, int dim);

#endif
