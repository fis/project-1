#include "cg.h"
#include "gmres.h"
#include "matrix.h"
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_ITER 1
#define CG "cg"
#define GMRES "gmres"

#define PROG_MSG                                                               \
  "Incorrect set of args.\
 Run in the following way:\n\n\
./matd <file_name> <method> <m>\n\n\
<file_name>: Name of the input file with matrix stored in MSR format.\n\
<method>: Method name [gmres, cg].\n\
<m>: Krylov subspace dimension.\n"

int check_method_validity(char *method) {
  if (strcmp(method, CG) == 0) {
    printf("=== CG ===\n");
    return 0;
  } else if (strcmp(method, GMRES) == 0) {
    printf("=== GMRES ===\n");
    return 0;
  } else {
    printf("Unknown method %s. Choose among: [%s, %s]\n", method, CG, GMRES);
    return -1;
  }
}

int main(int argc, char **argv) {
  int m;
  char *file_name, *method;
  if (argc != 4) {
    printf(PROG_MSG);
    return -1;
  } else {
    file_name = argv[1];
    if (check_method_validity(method = argv[2]) == -1)
      return -1;
    m = atoi(argv[3]);
  }
  bool f_cg = strcmp(method, CG) == 0;

  MATRIX_MSR *A;
  if ((A = parse_matrix_msr(file_name)) == NULL)
    return -1;

  int dim = A->matrix_dim;
  double *x_exact = get_filled_vector(dim, 1.0);
  double *b = get_zero_vector(dim);
  double *x_0 = get_zero_vector(A->matrix_dim);
  double *y = get_zero_vector(dim);
  matvec_msr(A, x_exact, b);
  matvec_msr(A, x_0, y);

  double R_0_norm = compute_residual(b, y, A->matrix_dim);
  double res_norm = 0.0;
  int n_iter = 0;
  do {
    printf("\nN    %d\n", n_iter);
    printf("m    r_m    r_0\n");
    double *x_m = f_cg ? cg(A, x_0, b, x_exact, m) : gmres(A, x_0, b, m);
    copy_vector(x_m, x_0, dim);
    free(x_m);

    matvec_msr(A, x_0, y);
    res_norm = compute_residual(b, y, dim);

    n_iter++;
  } while (res_norm / R_0_norm > EXIT_CONDITION && n_iter < MAX_ITER);

  free(x_0);
  free(x_exact);
  free(y);
  free(b);
  cleanup_msr(A);

  return 0;
}
