#include "cg.h"
#include "matrix.h"
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double *cg(MATRIX_MSR *A, double *x_0, double *b, double *x_exact, int m) {
  int dim = A->matrix_dim;

  // Allocate running x_m
  double *x_m = get_zero_vector(dim);
  copy_vector(x_0, x_m, dim);

  // Compute initial residual
  double *y_0 = get_zero_vector(dim);
  matvec_msr(A, x_0, y_0);
  double *r_0 = get_zero_vector(dim);
  substr_vectors(b, y_0, r_0, dim);

  // Allocate running residual
  double *r_m = get_zero_vector(dim);
  copy_vector(r_0, r_m, dim);
  double r_m_inner = 0.0;

  // Allocate conjugate direction
  double *p_m = get_zero_vector(dim);
  copy_vector(r_0, p_m, dim);

  // Allocate A*p_m vector
  double *A_p_m = get_zero_vector(dim);

  // Allocate err vector
  double *err = get_zero_vector(dim);
  double *err_A = get_zero_vector(dim);
  double err_norm_A = 0.0;

  /*
   * CG main body
   */
  double alpha, beta = 0.0;
  for (int i = 0; i < m; i++) {
    r_m_inner = inner_product(r_m, r_m, dim);

    // Compute error
    substr_vectors(x_m, x_exact, err, dim);
    matvec_msr(A, err, err_A);
    err_norm_A = sqrt(inner_product(err_A, err, dim));
    // Log convergence values
    printf("r    %d    %lf\n", i + 1, sqrt(r_m_inner));
    printf("e    %d    %lf\n", i + 1, err_norm_A);

    matvec_msr(A, p_m, A_p_m);

    alpha = r_m_inner / inner_product(A_p_m, p_m, dim);
    // x_m+1 = x_m + α*p_m
    combine_vectors_inplace(x_m, x_m, p_m, 1.0, alpha, dim);
    // r_m+1 = r_m - α*A_p_m
    combine_vectors_inplace(r_m, r_m, A_p_m, 1.0, -1 * alpha, dim);

    beta = inner_product(r_m, r_m, dim) / r_m_inner;
    // p_m+1 = r_m+1 + β*p_m
    combine_vectors_inplace(p_m, r_m, p_m, 1.0, beta, dim);
  }

  free(err);
  free(err_A);
  free(y_0);
  free(r_0);
  free(r_m);
  free(p_m);
  free(A_p_m);

  return x_m;
}
