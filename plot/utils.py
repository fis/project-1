DELIM = " " * 4


def parse_cg_convergence():
    file_name = "in/data_cg_5357.txt"
    err = []
    res = []
    try:
        with open(file_name, "r") as f:
            for _ in range(4):
                next(f)
            for line in f:
                data = line.split(DELIM)
                tag, val = data[0], data[2]
                if tag == "e":
                    err.append(float(val))
                else:
                    res.append(float(val))
            return err, res
    except Exception as e:
        raise Exception(f"Could not parse the data: {e}")


def parse_inner_product():
    file_name = "in/inner_product_512.txt"
    k = []
    v_prod = []
    try:
        with open(file_name, "r") as f:
            for line in f:
                data = line.split(DELIM)
                k.append(int(data[0]) + 1)
                v_1_k = abs(float(data[1]))
                v_prod.append(
                    v_1_k if v_1_k > 1e-10 else 1e-10
                )  # define the zero threshold
        return k, v_prod
    except Exception as e:
        raise Exception(f"Could not parse the data: {e}")


class ExecutionLog:
    def __init__(self, file_name: str):
        self.file_name = file_name
        self.N = 0
        self.t = 0.0
        self.m = []
        self.r_m = []
        self.r_0 = []
        try:
            self.parse()
        except Exception as e:
            raise Exception(f"Could not parse the data: {e}")

    def parse(self):
        with open(self.file_name, "r") as f:
            next(f)  # skip the first line
            for line in f:
                if line[0] == "N":
                    n_iter = int(line.split(DELIM)[1])
                    self.N = n_iter
                    next(f)  # skip the headers
                elif line[0] == "T":
                    t_msec = float(line.split(DELIM)[1])
                    self.t += t_msec
                elif line[0] == "\n":
                    self.m.append([])
                    self.r_m.append([])
                    self.r_0.append([])
                else:
                    current_iter = self.N
                    iter_data = line.split(DELIM)
                    self.m[current_iter].append(int(iter_data[0]))
                    self.r_m[current_iter].append(float(iter_data[1]))
                    self.r_0[current_iter].append(float(iter_data[2]))
