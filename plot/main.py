import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import ExecutionLog, parse_inner_product, parse_cg_convergence


def plot_residual(iter_d: ExecutionLog):
    m = np.array(iter_d.m)
    r_m = np.array(iter_d.r_m)
    r_0 = np.array(iter_d.r_0)

    sns.set_style("darkgrid", {"axes.facecolor": ".9"})

    plt.scatter(m, np.log10(r_m / r_0), linewidths=0.3, c="#457b9d", label="m=512")
    plt.xlabel("iter")
    plt.ylabel("log(r_m / r_0)")
    plt.title("Full GMRES relative residual")
    plt.legend()
    plt.show()


def plot_inner_product(k, v_prod):
    sns.set_style("darkgrid", {"axes.facecolor": ".9"})

    k = np.array(k)
    v_prod = np.array(v_prod)

    plt.scatter(k, np.log10(v_prod), linewidths=0.3, c="#457b9d")
    plt.xlabel("k")
    plt.ylabel("log( (v1, vk) )")
    plt.title("Orthogonality check of the Krylov basis")
    plt.show()


def main_gmres():
    k, v_prod = parse_inner_product()
    plot_inner_product(k, v_prod)

    m_list = [30, 50, 100, 150, 200, 250, 512]
    print(f"m    t[msec]    N")
    for m in m_list:
        file_name = f"in/data_{m}_timed.txt"
        exec_d = ExecutionLog(file_name)
        print(f"{m}    {exec_d.t}    {exec_d.N}")


def plot_cg_convergence(err, res):
    sns.set_style("darkgrid", {"axes.facecolor": ".9"})

    k = np.arange(len(res))
    err = np.array(err)
    res = np.array(res)

    plt.subplot(211)
    plt.title("Conjugate Gradient convergence parameters")
    plt.scatter(k, np.log10(err), linewidths=0.3, c="#457b9d")
    plt.ylabel("log( ||e_k||_A )")

    plt.subplot(212)
    plt.scatter(k, np.log10(res), linewidths=0.3, c="#a3b18a")
    plt.xlabel("k")
    plt.ylabel("log( ||r_k|| )")

    plt.show()


def main_cg():
    err, res = parse_cg_convergence()
    plot_cg_convergence(err, res)


if __name__ == "__main__":
    main_cg()
