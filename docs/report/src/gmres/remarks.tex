\subsection {Remarks}

In the following the full GMRES method without preconditioning is discussed.
An approximation to the solution without restarting, which satisfies
the tolerance of $1e^{-8}$ residual drop, is obtained in the Krylov
space of $m = 512$. The residual decrease dynamics can be seen on the
Figure \ref{fig:full_gmres_res}.

\begin{figure}
	\centering
	\includegraphics[scale=0.7]{img/gmres_512_rel_res_log.png}
	\caption{Logarithmic plot of residual ration versus iteration step.}
	\label{fig:full_gmres_res}
\end{figure}

It is worth noting, that 512 is the lower limit for the
dimensions of the Krylov space to solve the given system in a single 
sweep. However, it is not the most optimal parameter resource-wise.
Practically, it is more desirable to use GMRES in a restarted formulation
with a significantly lower dimension $m$.

In an effort to find a more optimal parameter $m$, a series of GMRES runs
are performed. The following table concisely summarizes the results of the
experiment:

\begin{center}
\begin{tabular}{ c c c }
	m & T[msec] & N \\ 
	30 & 748.0 & 198 \\
	50 & 483.0 & 52 \\
	100 & 542.0 & 16 \\
	150 & 590.0 & 8 \\
	200 & 647.0 & 5 \\
	250 & 792.0 & 4 \\
	512 & 787.0 & 1 
\end{tabular}
\end{center}

The time was measured with respect to CPU cycles, rather than the elapsed time.
Based on the measurements it can be seen that the most optimal value for $m$ occurs
in the vicinity of $m=50$. It is a surprising result, however, it supports a remark
made earlier - "\textit{Practically, it is more desirable to use GMRES in a restarted
formulation...}". The advantage in speed is primarily achieved due to cutting down on
iterations with quadratic complexity.
Apart from runtime considerations, restarted GMRES is also less memory demanding.
It requires less memory allocation due to smaller number of Krylov basis vectors to
be stored. Of course Hessenberg matrix also grows as the parameter $m$ gets larger,
but the memory requirements for it are way more benign in comparison to the
requirements for the Krylov basis set.

\begin{align*}
	& \text{mem}(H_{m}) = \text{sizeof}(double) \cdot m \cdot (m+1) \\
	& \text{mem}(V_{m+1}) = \text{sizeof}(double) \cdot (m+1) \cdot dim
\end{align*}

Where $dim$ is the original dimension of matrix $A$ in a sense 
$A \in \mathbf{R}^{dim \times dim}$, and $dim >> m$.


A sanity check for the correctness of Krylov space construction can be done
by inspecting the inner product of basis vectors $V_{m}$ forming the vector
space (see Figure \ref{fig:full_gmres_basis_orth}).

\begin{figure}
	\centering
	\includegraphics[scale=0.7]{img/inner_prod_512.png}
	\caption{Orthogonality of the basis vectors forming $K_{m}$.}
	\label{fig:full_gmres_basis_orth}
\end{figure}

Due to the finite capacity to represent real numbers in the floating point
form, there is a certain inherent truncation error for values around machine
epsilon. Thus, all values in the following range $\text{fabs}((v_{1}, v_{k})) < 1e^{-10}$
were considered equivalently to be zero. It can also be observed that corresponding
inner products are above considered zero in the index range of $\sim460 < k < 512$.
This could mean that approaching the limit of full GMRES newly added vectors
start gaining some small degree of linear dependence, be it a small dependence.


