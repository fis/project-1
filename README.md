# Krylov Space Methods

## General

This project implements two iterative methods to approximate the solution to the
linear system of the form Ax=b.

- GMRES
- Conjugate Gradients

## Build

To build the project and run it, you will need the `gcc` compiler
as well as GNU build tools present on your system to invoke `make`
command.
If you do not have those on your system just install the `build-essential`.

Once those requirements are satisfied perform the following steps:

```
$ cd build
$ make
```

These commands will produce an executable and debug information.
Then to run the solver issue the following command:

```
$ ./matd <msr_matrix_file> <method> <m>
```

- `<method>` refers to one of [gmres, cg]
- `<m>` Krylov space dimension

The program will produce a sequence of prints logging the progress
and convergence information. This information may be channeled to a
separate file e.g.


```
$ ./matd ../in/cg_matrix.txt cg 50000 > data.txt
```
